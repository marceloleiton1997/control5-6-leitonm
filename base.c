//# Leiton-Marcelo 
#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
//-------------------------------------------------------------------------------------------------//
//cumplimiento de algoritmos matematicos
float desvStd(estudiante curso[]){

	return 0.0;
}

float menor(float prom[]){
	int i, menor=prom[0];
	for (i=0; i <24; i++){
		if(menor>prom[i])
			menor=prom[i];
	printf("menor : %d\n",menor);

	}
	printf("menor : %d\n",menor);
	return 0.0;
}
float mayor(float prom[]){
	int i, mayor=prom[0];
	for (i=0; i <24; i++){
		if(mayor<prom[i])
			mayor=prom[i];
			printf("%d",mayor);

	}
	printf("mayor : %d\n",mayor);
	return 0.0;
}

//-------------------------------------------------------------------------------------------------//
//Registra las calificaciones obtenidas, por los estudiantes.  
// Además calcula el promedio final del estudiante.

void registroCurso(estudiante curso[]){
	int i = 0;
	float promedio=0;
	printf("\n\n#--#INGRESA LAS NOTAS ##--#\n\n");

//recorrer con ciclo for ---> para asignar las notas en las variables designadas para control y proyecto 

	for (i=0; i <24; i++){

		printf("\nIngrese Las Notas Para El Estudiante: %s %s %s\n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
		
		printf("\n\n***--> INGRESA CONTROLES <--***\n");

		printf("\n-> Ingrese Nota Del control 1: ");scanf("%f" ,&curso[i].asig_1.cont1);
		printf("\n-> Ingrese Nota Del control 2: ");scanf("%f" ,&curso[i].asig_1.cont2);
		printf("\n-> Ingrese Nota Del control 3: ");scanf("%f" ,&curso[i].asig_1.cont3);
		printf("\n-> Ingrese Nota Del control 4: ");scanf("%f" ,&curso[i].asig_1.cont4);
		printf("\n-> Ingrese Nota Del control 5: ");scanf("%f" ,&curso[i].asig_1.cont5);
		printf("\n-> Ingrese Nota del control 6: ");scanf("%f" ,&curso[i].asig_1.cont6);
		printf("\n //--> INGRESA PROYECTOS <--//\n");

		printf("\n-> Ingrese Nota Del Proyeto 1: ");scanf("%f" ,&curso[i].asig_1.proy1);
		printf("\n-> Ingrese Nota Del Proyeto 2: ");scanf("%f" ,&curso[i].asig_1.proy2);
		printf("\n-> Ingrese Nota Del Proyeto 3: ");scanf("%f" ,&curso[i].asig_1.proy3);

}
printf("\n*****--- PROMEDIOS ---****\n");
//recorrer con ciclo for ---> para cada uno de los estudiantes (asignar su promedio)
		for (i=0; i <24; i++){
//sumando variables , multiplicando por su porcentaje y dividiento por 100 ---> obtener promedio
			
			promedio=(((curso[i].asig_1.cont1*5)/100)+ ((curso[i].asig_1.cont2*5)/100)+ ((curso[i].asig_1.cont3*5)/100)+ ((curso[i].asig_1.cont4*5)/100) + ((curso[i].asig_1.cont5*5)/100)+((curso[i].asig_1.cont6*5)/100) + ((curso[i].asig_1.proy1*20)/100)+ ((curso[i].asig_1.proy2*20)/100)+ ((curso[i].asig_1.proy3*30)/100));
				printf("\nEl promedio es: %.1f",promedio);
					printf(" --------> Para el estudiante: %s %s %s\n",curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
// asignando condiciones para promedio 

					if (promedio >= 4 ){printf("--> aprobado <--\n");}
					if (promedio < 4){printf("--> reprobado <--\n");}
					curso[i].prom=promedio;			
		}
}

//-------------------------------------------------------------------------------------------------//

// debe clasificar segun estudiantes: aprobados , reprobados

//Almacena el registro de estudiantes en un archivo plano
//separado por tabulación.

void clasificarEstudiantes(char path[], estudiante curso[]){

	// se crea archvo con los aprobados
	
	FILE *aprobados;
	if((aprobados=fopen("aprobados.txt","w"))==NULL){
		printf("\nerror al crear el archivo");
		exit(0); 
	}
	else{
		int i=0;

		for (i=0; i <24; i++){
			if (curso[i].prom >= 4){

			fprintf(aprobados,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3,
	curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6, curso[i].prom);
	} 
}
	printf("\n **El registro de estudiantes aprobados fue almacenado de manera correcta**\n \n ");
	fclose(aprobados);

}
//-------------------------------------------------------------------------------------------------//

 // se crea archivo con los reprobados

	FILE *reprobado;
	if((reprobado=fopen("reprobado.txt","w"))==NULL){
		printf("\nerror al crear el archivo");
		exit(0); 
	}
	else{
		int i=0;

		for (i=0; i <24; i++){
			if (curso[i].prom < 4){

			fprintf(reprobado,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3,
	curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6, curso[i].prom);
	} 
}
	printf("\n **El registro de estudiantes reprobados fue almacenado de manera correcta**\n \n ");
	fclose(reprobado);
}
}

//-------------------------------------------------------------------------------------------------//

// Calcula métricas de dispersión y las presenta en pantalla.

void metricasEstudiantes(estudiante curso[]){
	int i = 0;
	float promedio=0;
	for (i=0; i <24; i++){
//-----> sumando las variables y dividiendo por su total se obtiene el promedio
			
		promedio=(((curso[i].asig_1.cont1*5)/100)+ ((curso[i].asig_1.cont2*5)/100)+ ((curso[i].asig_1.cont3*5)/100)+ ((curso[i].asig_1.cont4*5)/100) + ((curso[i].asig_1.cont5*5)/100)+((curso[i].asig_1.cont6*5)/100) + ((curso[i].asig_1.proy1*20)/100)+ ((curso[i].asig_1.proy2*20)/100)+ ((curso[i].asig_1.proy3*30)/100));
			printf("<---//------//----//------//--->");
			printf("\nEl promedio es: %.1f",promedio);
			printf(" --------> Para el estudiante: %s %s %s\n",curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
			if (promedio >= 4 ){printf("\n--> aprobado <--\n");}
			if (promedio < 4){printf("\n--> reprobado <--\n");}
			curso[i].prom=promedio;
	}

}
//-------------------------------------------------------------------------------------------------//

// presenta en pantalla las opciones

void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );

        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros    ok
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso    ok
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);          //  ok 
					break;

            case 5: clasificarEstudiantes("destino.txt", curso);// clasificar los estudiantes segun reprobados y aprobados 
            		break;
         }

    } while ( opcion != 6 );
}
//-------------------------------------------------------------------------------------------------//
int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}